<?php
if (isset($_POST)) {
    require 'lib.php';
 
    $id = $_POST['id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $address = $_POST['address'];
    $tel = $_POST['tel'];
    $email = $_POST['email'];
    $psw = $_POST['psw'];
 
    $object = new CRUD();
 
    $object->Update($first_name, $last_name, $address, $tel, $email, $psw, $id);
}