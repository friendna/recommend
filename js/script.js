// Add Record
function addRecord() {
  // get values
  var first_name = $("#first_name").val();
  first_name = first_name.trim();
  var last_name = $("#last_name").val();
  last_name = last_name.trim();
  var address = $("#address").val();
  address = address.trim();
  var tel = $("#tel").val();
  tel = tel.trim();
  var email = $("#email").val();
  email = email.trim();
  var psw = $("#psw").val();
  psw = psw.trim();

  if (first_name == "") {
      alert("First name field is required!");
  }
  else if (last_name == "") {
      alert("Last name field is required!");
  }
  else if (address == "") {
    alert("Address field is required!");
  }
  else if (tel == "") {
    alert("Telephone number field is required!");
  }
  else if (email == "") {
      alert("Email field is required!");
  }
  else if (psw == "") {
    alert("Password field is required!");
  }
  else {
      // Add record
      $.post("ajax/createUser.php", {
          first_name: first_name,
          last_name: last_name,
          address: address,
          tel: tel,
          email: email,
          psw: psw
      }, function (data, status) {
          // close the popup
          $("#add_new_record_modal").modal("hide");

          // read records again
          readRecords();

          // clear fields from the popup
          $("#first_name").val("");
          $("#last_name").val("");
          $("#address").val("");
          $("#tel").val("");
          $("#email").val("");
          $("#psw").val("");
      });
  }
}

// READ records
function readRecords() {
  $.get("ajax/readUser.php", {}, function (data, status) {
      $(".records_content").html(data);
  });
}

function GetUserDetails(id) {
  // Add User ID to the hidden field
  $("#hidden_user_id").val(id);
  $.post("ajax/detailsUser.php", {
          id: id
      },
      function (data, status) {
          // PARSE json data
          var user = JSON.parse(data);
          // Assign existing values to the modal popup fields
          $("#update_first_name").val(user.first_name);
          $("#update_last_name").val(user.last_name);
          $("#update_address").val(user.address);
          $("#update_tel").val(user.tel);
          $("#update_email").val(user.email);
          $("#update_psw").val(user.psw);
      }
  );
  // Open modal popup
  $("#update_user_modal").modal("show");
}

function UpdateUserDetails() {
  // get values
  var first_name = $("#update_first_name").val();
  first_name = first_name.trim();
  var last_name = $("#update_last_name").val();
  last_name = last_name.trim();
  var address = $("#update_address").val();
  address = address.trim();
  var tel = $("#update_tel").val();
  tel = tel.trim();
  var email = $("#update_email").val();
  email = email.trim();
  var psw = $("#update_psw").val();
  psw = psw.trim();

  if (first_name == "") {
      alert("First name field is required!");
  }
  else if (last_name == "") {
      alert("Last name field is required!");
  }
  else if (address == "") {
    alert("Address field is required!");
  }
  else if (tel == "") {
    alert("Telephone number field is required!");
  }
  else if (email == "") {
      alert("Email field is required!");
  }
  else if (psw == "") {
    alert("Password field is required!");
}
  else {
      // get hidden field value
      var id = $("#hidden_user_id").val();

      // Update the details by requesting to the server using ajax
      $.post("ajax/updateUser.php", {
              id: id,
              first_name: first_name,
              last_name: last_name,
              address: address,
              tel: tel,
              email: email,
              psw: psw
          },
          function (data, status) {
              // hide modal popup
              $("#update_user_modal").modal("hide");
              // reload Users by using readRecords();
              readRecords();
          }
      );
  }
}

function DeleteUser(id) {
  var conf = confirm("Are you sure, do you really want to delete User?");
  if (conf == true) {
      $.post("ajax/deleteUser.php", {
              id: id
          },
          function (data, status) {
              // reload Users by using readRecords();
              readRecords();
          }
      );
  }
}

$(document).ready(function () {
  // READ records on page load
  readRecords(); // calling function
});